import 'ee/trials/country_select';
import { trackSaasTrialSubmit, trackSaasTrialSkip } from '~/google_tag_manager';

trackSaasTrialSubmit();
trackSaasTrialSkip();
